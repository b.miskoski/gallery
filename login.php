<?php

    session_start();
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {

        header('Location: index.php');
        exit;
    }

    require_once 'config.php';

    $email_err = $password_err = '';
    $email = $password = '';

    if($_SERVER['REQUEST_METHOD'] == 'POST') {

        if(empty($_POST['email'])) {
            $email_err = 'Please enter your email!';
        } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $email_err = 'Please enter valid email address!';
        } else {
            $email = $_POST['email'];
        }

        if(empty($_POST['password'])) {
            $password_err = 'Please enter password!';
        } else {
            $password = $_POST['password'];
        }

        if(empty($email_err) && empty($password_err)) {

            $sql = 'SELECT id, email, password FROM users WHERE email = :email';

            if($stmt = $pdo->prepare($sql)) {

                $stmt->bindParam(':email', $param_email);

                $param_email = $email;

                if($stmt->execute()) {
                    if($stmt->rowCount() == 1) {
                        if($row = $stmt->fetch()) {

                            $id = $row['id'];
                            $email = $row['email'];
                            $hashed_password = $row['password'];
    
                            if(password_verify($password, $hashed_password)) {
    
                                session_start();
                                $_SESSION['loggedin'] = true;
                                $_SESSION['id'] = $id;
                                $_SESSION['email'] = $email;
                                header('Location: index.php');
                            } else {
                                $password_err = 'The password you entered is not correct!';
                            }
                        }
                    } else {
                        $email_err = 'No user with that email!';
                    }
                } else {
                    echo 'Something went wrong!';
                }
                unset($stmt);
            }
        }
        unset($pdo);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet"> 
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">
    <title>Gallery | Sign In</title>
</head>
<body>
    <section>
        <div class="signin-register-wrapper d-flex align-items-center">
            <div class="form d-flex flex-direction-column">
                <div class="form-title-wapper mb-20 d-flex align-items-center justify-content-between">
                    <p class="form-title"><span class="border-bottom-gradient">Sign</span> In</p>
                   <a href="index.php"><img class="login-logo" src="assets/img/logo-albums.png" alt="logo"></a>
                </div>
                <form class="signin-register-form mb-20" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="mb-5 <?= (!empty($email_err)) ? 'error-input' : ''; ?>">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="<?= $email; ?>">
                        <span class="err-msg"><?= $email_err; ?></span>
                    </div>
                    <div class="mb-35 <?= (!empty($password_err)) ? 'error-input' : ''; ?>">
                        <label for="password">Password</label>
                        <input type="password" name="password" value="<?= $password; ?>">
                        <span class="err-msg"><?= $password_err; ?></span>
                    </div>
                    <button type="submit" class="btn btn-gradient">Login</button>
                </form>
                <span class="form-question">Don't have and account? <a class="text-underline" href="register.php">Register here.</a></span>
            </div>
            <div class="form-text d-flex align-items-center justify-content-center" style="background: url('assets/img/login-bg.jpg')center center/cover;">
                <h1 class="banner-title text-right text-white">Your <br> <span class="border-bottom-gradient">memories</span> <br> are <span class="border-bottom-gradient">safe</span> with us</h1>
            </div>
        </div>
    </section>
</body>
</html>