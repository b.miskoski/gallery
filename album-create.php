<?php

    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] === false) {
        header('location: login.php');
        exit;
    }

    require_once 'config.php';

    $album_title_err = $album_description_err = $album_cover_err = '';
    $album_title = $album_description = '';

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        
        if(empty($_POST['album_title'])) {
            $album_title_err = 'Please enter album title!';
        } else {
            $album_title = filter_var($_POST['album_title'], FILTER_SANITIZE_STRING);
        }

        if(empty($_POST['album_description'])) {
            $album_description_err = 'Please enter album description!';
        } else {
            $album_description = filter_var($_POST['album_description'], FILTER_SANITIZE_SPECIAL_CHARS);
        }

        if (isset($_FILES['cover']) && !empty($_FILES['cover']['name'])) {

            if(empty($album_title_err) && empty($album_description_err)) {
                $pathInfo = pathinfo($_FILES['cover']['name']);
                $fileBaseName = $pathInfo['filename'];
                $fileExt = $pathInfo['extension'];
                $uploadDir = "assets/img";
                $index = 1;
                $fullName = $fileBaseName . "." . $fileExt;
                while (file_exists($uploadDir . "/" . $fullName)) {
                    $fullName = $fileBaseName . "-" . $index . "." . $fileExt;
                    $index++;
                }
                move_uploaded_file($_FILES['cover']['tmp_name'],
                $uploadDir . "/" . $fullName);
            }
        } else {
            $album_cover_err = 'Album cover required!';
        }


        if(empty($album_title_err) && empty($album_description_err) && empty($album_cover_err)) {

            $sql = 'INSERT INTO albums (title, description, cover) VALUES (:title, :description, :cover)';

            if($stmt = $pdo->prepare($sql)) {

                $stmt->bindParam(':title', $param_album_title);
                $stmt->bindParam(':description', $param_album_description);
                $stmt->bindParam(':cover', $param_album_cover);

                $param_album_title = $album_title;
                $param_album_description = $album_description;
                $param_album_cover = $fullName;

                if($stmt->execute()) {
                    $id = $pdo->lastInsertId();
                    $_SESSION['message'] = 'Album created successfully!';
                    header('Location: album-upload.php?id='.$id);

                    
                } else {
                    echo 'Something went wrong';
                }
                unset($stmt);
            }

        }

        unset($pdo);

    }

?>

<?php require_once 'partials/site/header.php'; ?>

    <section class="create-album">
            <div class="h-100 d-flex flex-direction-column justify-space-between">
                <div class="bg-rectangles align-self-end"></div>
                <div class="bg-rectangles align-self-ceter"></div>
                <div class="bg-rectangles align-self-start"></div>
            </div>
            <div class="create-form">
                <p class="create-form-title mb-20"><span class="border-bottom-gradient">Create</span> album</p>
                <form class="signin-register-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
                    <div class="mb-5 <?= (!empty($album_title_err)) ? 'error-input' : ''; ?>">
                        <label for="album_title">Album title</label>
                        <input type="text" name="album_title" value="<?= $album_title; ?>">
                        <span class="err-msg"><?= $album_title_err; ?></span>
                    </div>
                    <div class="mb-5 <?= (!empty($album_description_err)) ? 'error-input' : ''; ?>">
                        <label for="album_title">Album description</label>
                        <textarea name="album_description"><?= $album_description; ?></textarea>
                        <span class="err-msg"><?= $album_description_err; ?></span>
                    </div>
                    <div class="mb-30">
                        <label for="cover">Album cover photo</label>
                        <input type="file" name="cover" accept="image/*">
                        <span class="err-msg"><?= $album_cover_err; ?></span>
                    </div>
                    <button type="submit" class="btn btn-gradient">Create</button>
                </form>
            </div>
    </section>

<?php require_once 'partials/site/footer.php'; ?>