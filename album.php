<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] === false) {
        header('location: login.php');
        exit;
    }
?>
<?php 
    require_once 'config.php';
    $id = trim($_GET['id']);
    $sql = 'SELECT * FROM images WHERE album_id = :id';

    $result = $pdo->prepare($sql);

    $result->bindParam(':id', $param_id);

    $param_id = $id;
?>
<?php require_once 'partials/site/header.php'; ?>
    <?php if($result->execute()): ?>
        <?php 
            $row = $result->fetchAll();
        ?>
            <?php if($result->rowCount() > 0): ?>
                <section class="images-showcase d-flex flex-direction-column justify-space-between">
                    <div class="image-preview">
                        <img id="current" src="assets/img/album-images/<?= $row[0][1];?>" alt="test">
                    </div>
                    <div class="carousel-container">
                        <div class="carousel-inner">
                            <div class="track">
                                <?php foreach($row as $img) : ?>
                                    <div class="image-container">
                                        <div class="image">
                                            <img class="img-fluid" src="assets/img/album-images/<?= $img['name']; ?>" alt="test">
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="slider-nav <?= ($result->rowCount() <= 5) ? 'd-none' : ''; ?>">
                            <button class="prev"><i class="fa fa-arrow-left"></i></button>
                            <button class="next"><i class="fa fa-arrow-right"></i></button>
                        </div>
                    </div>
                </section>
            <?php else: ?>
                <section class="d-flex justify-content-center p-50">   
                    <div class="text-center">
                        <p class="mb-35">No images in the album <i class="fa fa-frown-o" aria-hidden="true"></i></p>
                        <a class="btn btn-gradient" href="album-upload.php?id=<?= $_GET['id']; ?>">Upload images</a>
                    </div>
                </section>
            <?php endif; ?>
            <?php else: ?>
                <?php echo 'Something went wrong'; ?>
            <?php endif; ?>
    <script>
        const prev  = document.querySelector('.prev');
        const next = document.querySelector('.next');
        const track = document.querySelector('.track');

        const carouselWidth = document.querySelector('.carousel-container').offsetWidth;
        const imageContainerWidth = document.querySelector('.image-container').offsetWidth;

        let index = 0;

        next.addEventListener('click', () => {
            index++;
            prev.classList.add('show');
            track.style.transform = `translateX(-${index * imageContainerWidth}px)`;

            if(track.offsetWidth - (index * imageContainerWidth) < carouselWidth) {
                next.classList.add('hide');
            }
        })

        prev.addEventListener('click', () => {
            index--;
            next.classList.remove('hide');
            if(index === 0) {
                prev.classList.remove('show');
            }
            track.style.transform = `translateX(-${index * imageContainerWidth}px)`;
        })

        const current = document.querySelector('#current');
        const images = document.querySelectorAll('.image img');
        const opacity = 0.6;

        images[0].style.opacity = opacity;

        images.forEach(img =>
            img.addEventListener('click', imgClick)
        );

        function imgClick(e) {

            images.forEach(img => (img.style.opacity = 1));

            current.src = e.target.src;

            current.classList.add('fade-in');

            setTimeout(() => current.classList.remove('fade-in'), 100);

            e.target.style.opacity = opacity;
        }

    </script>

<?php require_once 'partials/site/footer.php'; ?>