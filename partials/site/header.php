<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Sriracha&display=swap" rel="stylesheet"> 
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">
    <title>Gallery | </title>
</head>
<body>

    <header>
        <nav class="d-flex align-items-center justify-content-between">
            <div>
                <ul class="links-list d-flex align-items-center">
                    <li><a href="index.php"><img class="logo" src="assets/img/logo-albums.png" alt="logo"></a></li>
                    <li><a class="border-link-bottom-gradient <?= (basename($_SERVER['SCRIPT_FILENAME']) === 'album-create.php') ? 'active' : ''; ?>" href="album-create.php">Create</a></li>
                    <li><a class="border-link-bottom-gradient <?= (basename($_SERVER['SCRIPT_FILENAME']) === 'album-upload.php') ? 'active' : ''; ?>" href="album-upload.php">Upload</a></li>
                </ul>
            </div>
            <div>
                <p class="account"><span class="border-link-bottom-gradient">My account</span></p>
                <div>
                    <ul class="profile-links">
                        <li><a class="border-link-bottom-gradient" href="#">Profile</a></li>
                        <li><a class="border-link-bottom-gradient" href="logout.php">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>