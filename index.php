<?php

    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] === false) {
        header('location: login.php');
        exit;
    }

?>

<?php 
    require_once 'partials/site/header.php'; 
    require_once 'config.php'; 
    $sql = 'SELECT albums.id, albums.title, albums.description, albums.cover, albums.created_at, COUNT(images.album_id) AS albumImg FROM albums LEFT JOIN images on images.album_id = albums.id GROUP BY albums.id';
?>

<section id="albums" class="p-50">
    <div class="container">
        <p class="form-title mb-50"><span class="border-bottom-gradient">Your</span> albums</p>
        <div class="row">
        <?php if($result = $pdo->query($sql)): ?>
            <?php if($result->rowCount() > 0): ?>
                <?php while($row = $result->fetch(PDO::FETCH_ASSOC)): ?>
                    <div class="album-card col-3 mr-45 mb-50">
                        <div class="album-cover">
                            <img class="img-fluid" src="assets/img/<?= $row['cover']; ?>" alt="album-cover">
                            <span class="info"><i class="fa fa-info-circle"></i></span>
                            <span style="display: none;"><?= $row['title']; ?></span>
                        </div>
                        <div class="text-center">
                            <p class="album-title"><a href="album.php?id=<?= $row['id'] ?>" class="border-link-bottom-gradient"><?= $row['title']; ?></a></p>
                            <span class="album-description" style="display: none;"><?= $row['description']; ?></span>
                            <span class="created-at" style="display: none;"><?= $row['created_at']; ?></span> 
                            <span class="count-img" style="display: none;"><?= $row['albumImg']; ?></span> 
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php else: ?>
            <div class="w-100 text-center">   
                <p class="mb-35">No albums created <i class="fa fa-frown-o" aria-hidden="true"></i></p>
                <a class="btn btn-gradient" href="album-create.php">Create album</a>
            </div>
            <?php endif; ?>
        <?php endif; ?>
        </div>
    </div>
</section>
<div class="modal"></div>
<script>

    let modal = document.querySelector('.modal');
    let infoIcon = document.querySelectorAll('.info');
    let mainSection = document.getElementById('albums');

    infoIcon.forEach(info =>
            info.addEventListener('click', (e) => {
                
                let albumTitle = e.target.parentNode.parentNode.nextElementSibling.children[0].textContent;
                let albumDescription = e.target.parentNode.parentNode.nextElementSibling.children[1].textContent;
                let createdAt = e.target.parentNode.parentNode.nextElementSibling.children[2].textContent;
                let countImg = e.target.parentNode.parentNode.nextElementSibling.children[3].textContent;
                
                let modalMessage = document.createElement('div');
                modalMessage.classList.add('info-modal');

                let closeBtn = document.createElement('span');
                closeBtn.innerHTML = 'x';
                closeBtn.classList.add('close-btn');

                let infoContainer = document.createElement('div');
                infoContainer.innerHTML = `
                <span class="border-link-bottom-gradient">Title: ${albumTitle}</span> <br> 
                <span class="">Description: ${albumDescription}</span> <br> 
                <span class="border-link-bottom-gradient">Created at: ${createdAt}</span> <br>
                <span class="border-link-bottom-gradient">Images: ${countImg}</span>`;

                modalMessage.append(closeBtn, infoContainer);

                modal.appendChild(modalMessage, infoContainer);

                mainSection.classList.add('blur');

                closeBtn.addEventListener('click', () => {
                    modalMessage.classList.add('d-none');
                    mainSection.classList.remove('blur');
                })
                
            })
    );

</script>

<?php require_once 'partials/site/footer.php'; ?>