<?php 

    session_start();
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

        header('Location: index.php');
        exit;
    }

    require_once 'config.php';

    $full_name_err = $email_err = $password_err = $confirm_password_err = '';
    $full_name = $email = $password = $confirm_password = '';

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        
        if(empty($_POST['full_name'])) {
            $full_name_err = 'Please enter your full name!';
        } elseif (!filter_var($_POST['full_name'], FILTER_VALIDATE_REGEXP, array('options' => array("regexp"=>"/^[a-zA-Z ]*$/")))) {
            $full_name_err = 'Please enter valid name format!';
        } else {
            $full_name = $_POST['full_name'];
        }

        if(empty($_POST['email'])) {
            $email_err = 'Please enter your email!';
        } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $email_err = 'Please enter valid email address!';
        } else {
            $email = $_POST['email'];
        }

        if(empty($_POST['password'])) {
            $password_err = 'Please enter password!';
        } elseif (strlen($_POST['password']) < 8) {
            $password_err = 'Password must contain 8 characters!';
        } else {
            $password = trim($_POST['password']);
        }

        if(empty($_POST['confirm_password'])) {
            $confirm_password_err = 'Please confirm password!';
        } else {
            $confirm_password = trim($_POST['confirm_password']);

            if(empty($password_err) && ($password != $confirm_password)) {
                $confirm_password_err = 'Passwords did not match!';
            }
        }

        if(empty($full_name_err) && empty($email_err) && empty($password_err) && empty($confirm_password_err)) {

            $sql = 'INSERT INTO users (full_name, email, password) VALUES (:full_name, :email, :password)';

            if($stmt = $pdo->prepare($sql)) {

                $stmt->bindParam(':full_name', $param_full_name);
                $stmt->bindParam(':email', $param_email);
                $stmt->bindParam(':password', $param_password);

                $param_full_name = $full_name;
                $param_email = filter_var($email, FILTER_VALIDATE_EMAIL, FILTER_SANITIZE_EMAIL);
                $param_password = password_hash($password, PASSWORD_DEFAULT);

                if($stmt->execute()) {
                    header('Location: login.php');
                } else {
                    echo 'Something went wrong';
                }
                unset($stmt);
            }
        }
        unset($pdo);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet"> 
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">
    <title>Gallery | Register</title>
</head>
<body>
    <section>
        <div class="signin-register-wrapper d-flex align-items-center">
            <div class="form-text pl-150 d-flex align-items-center" style="background: url('assets/img/register-bg.jpg')center top/cover;">
                <h1 class="banner-title text-left text-white"><span class="border-bottom-gradient">Entrusts us </span> <br> with your <br><span class="border-bottom-gradient">memories</span></h1>
            </div>
            <div class="form d-flex flex-direction-column">
                <div class="form-title-wapper mb-20 d-flex align-items-center justify-content-between">
                    <p class="form-title"><span class="border-bottom-gradient">Regi</span>ster</p>
                   <a href="index.php"><img class="login-logo" src="assets/img/logo-albums.png" alt="logo"></a>
                </div>
                <form class="signin-register-form mb-20" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="mb-5 <?= (!empty($full_name_err)) ? 'error-input' : ''; ?>">
                        <label for="name">Full name</label>
                        <input type="text" name="full_name" value="<?= $full_name; ?>">
                        <span class="err-msg"><?= $full_name_err; ?></span>
                    </div>
                    <div class="mb-5 <?= (!empty($email_err)) ? 'error-input' : ''; ?>">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="<?= $email; ?>">
                        <span class="err-msg"><?= $email_err; ?></span>
                    </div>
                    <div class="mb-5 <?= (!empty($password_err)) ? 'error-input' : ''; ?>">
                        <label for="password">Password</label>
                        <input type="password" name="password" value="<?= $password; ?>">
                        <span class="err-msg"><?= $password_err; ?></span>
                    </div>
                    <div class="mb-35 <?= (!empty($confirm_password_err)) ? 'error-input' : ''; ?>">
                        <label for="password">Confirm password</label>
                        <input type="password" name="confirm_password" value="<?= $confirm_password; ?>">
                        <span class="err-msg"><?= $confirm_password_err; ?></span>
                    </div>
                    <button type="submit" class="btn btn-gradient">Register</button>
                </form>
                <span class="form-question">Already have an account? <a class="text-underline" href="login.php">Sign in here.</a></span>
            </div>
        </div>
    </section>
</body>
</html>