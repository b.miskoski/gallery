<?php

    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] === false) {
        header('location: login.php');
        exit;
    }

    require_once 'config.php';

    $album_title_id_err = $album_name_err = '';

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        
        if(empty($_POST['album'])) {
            $album_title_id_err = 'Please select album!';
        } else {
            $album_title_id = trim($_POST['album']);
        }

        if(!empty(array_filter($_FILES['images']['name']))) {
            
            if(empty($album_title_id_err)) {

                $targetDir = "assets/img/album-images";
                foreach($_FILES['images']['name'] as $key => $val) { 

                    $imageName = pathinfo($_FILES['images']['name'][$key]);
                    $imageTitle = $imageName['filename'];
                    $imageExt = $imageName['extension'];

                    $fileName = basename($_FILES['images']['name'][$key]); 
                    $index = 1;

                    while (file_exists($targetDir . '/' . $fileName)) {
                        $fileName = $imageTitle .'-' . $index . '.' . $imageExt;
                        $index++;
                    }

                    $targetFilePath = $targetDir . '/' . $fileName;

                    move_uploaded_file($_FILES["images"]["tmp_name"][$key], $targetFilePath);

                    $sql = 'INSERT INTO images (name, album_id) VALUES (:name, :album_id)';

                    if($stmt = $pdo->prepare($sql)) {
                        
                        $stmt->bindParam(':name', $param_name);
                        $stmt->bindParam(':album_id', $param_album_id);

                        $param_name = $fileName;
                        $param_album_id = $album_title_id;

                        if($stmt->execute()) {
                            header('Location: album.php?id='.$album_title_id);
                        } else {
                            echo 'Something went wrong!';
                        }
                        unset($stmt);
                    }
                }      
            }
        } else {
            $album_name_err = 'Please upload images!';
        }
    }
?>

<?php require_once 'partials/site/header.php'; ?>

<?php $sql = 'SELECT * FROM albums'; ?>

    <section class="create-album">
            <div class="h-100 d-flex flex-direction-column justify-space-between">
                <div class="bg-rectangles align-self-start"></div>
                <div class="bg-rectangles align-self-ceter"></div>
                <div class="bg-rectangles align-self-end"></div>
            </div>
            <div class="create-form">
                <div class="message-div mb-50 <?= (isset($_SESSION['message'])) ? 'alert-message' : ''; ?>">
                    <p class="" ><?= (isset($_SESSION['message'])) ? $_SESSION['message'] . '<i class="x-btn align-self-center fa fa-times"></i>' : ''; ?></p>
                    <?php unset($_SESSION['message']); ?>
                </div>
                <div class="form-wrapper">
                    <p class="create-form-title mb-20"><span class="border-bottom-gradient">Upload</span> images</p>
                    <form class="signin-register-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
                        <div class="mb-5 <?= (!empty($album_title_id_err)) ? 'error-input' : ''; ?>">
                            <label for="album_title">Choose an album</label>
                            <select name="album" id="course">
                                <?php if($result = $pdo->query($sql)): ?>
                                    <?php if($result->rowCount() > 0): ?>    
                                        <?php while($row = $result->fetch()): ?>
                                            <option <?= (isset($_GET['id'])) ? (($row['id'] == $_GET['id']) ? 'selected' : '') : ''; ?> value="<?= $row['id']; ?>"><?= $row['title'] ?></option>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php unset($pdo); ?>
                            </select>
                            <span class="err-msg"><?= $album_title_id_err; ?></span>
                        </div>
                        <div class="mb-30">
                            <label for="cover">Upload images</label>
                            <input type="file" name="images[]" accept="image/*" multiple="multiple">
                            <span class="err-msg"><?= $album_name_err; ?></span>
                        </div>
                        <button type="submit" class="btn btn-gradient">Upload</button>
                    </form>
                </div>
            </div>
    </section>

<?php require_once 'partials/site/footer.php'; ?>

<script>

    let modal = document.getElementsByClassName('message-div')[0];                                      
    let closeBtn = document.getElementsByClassName('x-btn')[0];

    closeBtn.addEventListener('click', () => {
        modal.classList.add('d-none');
    })

    setTimeout(function() {
        modal.classList.add('d-none');
    },4000);


</script>